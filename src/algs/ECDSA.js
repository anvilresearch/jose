'use strict'

/**
 * Dependencies
 * @ignore
 */
const BaseAlgorithm = require('./BaseAlgorithm')

/**
 * ECDSA
 */
class ECDSA extends BaseAlgorithm {}

/**
 * Export
 */
module.exports = ECDSA
